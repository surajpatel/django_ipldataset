from django.urls import path
from . import views

# contain all the url of ipldataset projects

urlpatterns = [
    # path for problem one ex: /ipldataset/problem_one/
    path('',views.index, name = 'index'),
    path('problems/one', views.problem_one, name = 'problem_one'),
    path('problems/one/graph', views.problem_one_template, name = 'problem_one_template'),
    path('problems/two', views.problem_two, name = 'problem_two'),
    path('problems/two/graph', views.problem_two_template, name = 'problem_two_template'),
    path('problems/three', views.problem_three, name = 'problem_three'),
    path('problems/three/graph', views.problem_three_template, name = 'problem_three_template'),
    path('problems/four', views.problem_four, name = 'problem_four'),
    path('problems/four/graph', views.problem_four_template, name = 'problem_four_template'),
    path('problems/five/<int:year>/', views.problem_five, name = 'problem_five'),
    path('problems/five/<int:year>/graph', views.problem_five_template, name = 'problem_five_template'),
    path('matches', views.matches, name = 'matches'),
    path('matches/<int:match_id>/', views.match_details, name = 'match_details'),
    path('matches/seasons/<int:season>/', views.match_details_by_season, name = 'match_details_by_season'),
    path('deliveries', views.deliveries, name = 'deliveries'),
    path('deliveries/<int:delivery_id>/', views.delivery_details, name = 'delivery_details'),
]