from django.db import models
from postgres_copy import CopyManager

# Create your models here.

# model for matches table

class Matches(models.Model):

    id              = models.AutoField(primary_key=True )
    season          = models.CharField(max_length = 20, null = True)
    city            = models.CharField(max_length = 60, null = True)
    date            = models.CharField(max_length = 30, null = True)
    team1           = models.CharField(max_length = 60, null = True)
    team2           = models.CharField(max_length = 60, null = True)
    toss_winner     = models.CharField(max_length = 60, null = True)
    toss_decision   = models.CharField(max_length = 60, null = True)
    result          = models.CharField(max_length = 60, null = True)
    dl_applied      = models.IntegerField( null = True)
    winner          = models.CharField(max_length = 60, null = True)
    win_by_runs     = models.IntegerField(null = True)
    win_by_wickets  = models.IntegerField(null = True)
    player_of_match = models.CharField(max_length = 60, null = True)
    venue           = models.CharField(max_length = 60, null = True)
    umpire1         = models.CharField(max_length = 30, null = True)
    umpire2         = models.CharField(max_length = 30, null = True)
    umpire3         = models.CharField(max_length = 30, null = True)
    objects         = CopyManager()

    def __str__(self):

        return str(self.id)

# model for delivery table
class Deliveries(models.Model):

    match_id            = models.ForeignKey(Matches, on_delete=models.CASCADE)
    inning              = models.IntegerField(null = True, blank = True)
    batting_team        = models.CharField(max_length = 60, null = True)
    bowling_team        = models.CharField(max_length = 60, null = True)
    over                = models.IntegerField(null = True)
    ball                = models.IntegerField(null = True)
    batsman             = models.CharField(max_length = 60, null = True)
    non_striker         = models.CharField(max_length = 60, null = True)
    bowler              = models.CharField(max_length = 60, null = True)
    is_super_over       = models.IntegerField(null = True)
    wide_runs           = models.FloatField(null = True)
    bye_runs            = models.IntegerField( null = True)
    legbye_runs         = models.IntegerField( null = True)
    noball_runs         = models.FloatField( null = True)
    penalty_runs        = models.IntegerField( null = True)
    batsman_runs        = models.FloatField( null = True)
    extra_runs          = models.IntegerField(null = True)
    total_runs          = models.IntegerField(null = True)
    player_dismissed    = models.CharField(max_length = 60, null = True)
    dismissal_kind      = models.CharField(max_length = 60, null = True)
    fielder             = models.CharField(max_length = 60, null = True)
    objects             = CopyManager()


    def __str__(self):

        return str(self.match_id)