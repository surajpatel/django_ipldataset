# Generated by Django 2.2.6 on 2019-10-09 05:43

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Matches',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('season', models.CharField(max_length=20, null=True)),
                ('city', models.CharField(max_length=60, null=True)),
                ('date', models.CharField(max_length=30, null=True)),
                ('team1', models.CharField(max_length=60, null=True)),
                ('team2', models.CharField(max_length=60, null=True)),
                ('toss_winner', models.CharField(max_length=60, null=True)),
                ('toss_decision', models.CharField(max_length=60, null=True)),
                ('result', models.CharField(max_length=60, null=True)),
                ('dl_applied', models.IntegerField(null=True)),
                ('winner', models.CharField(max_length=60, null=True)),
                ('win_by_runs', models.IntegerField(null=True)),
                ('win_by_wickets', models.IntegerField(null=True)),
                ('player_of_match', models.CharField(max_length=60, null=True)),
                ('venue', models.CharField(max_length=60, null=True)),
                ('umpire1', models.CharField(max_length=30, null=True)),
                ('umpire2', models.CharField(max_length=30, null=True)),
                ('umpire3', models.CharField(max_length=30, null=True)),
            ],
        ),
    ]
