import csv
from django.core.management import BaseCommand
from ipldataset.models import Deliveries
from django.db import transaction

# overwrite method in BaseCommand


class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **kwargs):
        file_path = '/home/suraj/project/product/ipldataset/management/commands/deliveries.csv'
        # Since the CSV headers match the model fields,
        # you only need to provide the file's path (or a Python file object)
        insert_count = Deliveries.objects.from_csv(file_path)
        print("{} records inserted".format(insert_count))
