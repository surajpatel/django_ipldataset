# Generated by Django 2.2.6 on 2019-10-12 07:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ipldataset', '0002_deliveries'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matches',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
