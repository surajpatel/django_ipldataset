from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.template import loader
from .models import Matches, Deliveries
from django.db.models import *
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

# home view
def index(request):
    return render(request, 'ipldataset/index.html' )

# api for problem_one
@cache_page(CACHE_TTL)
def problem_one(request):
    no_of_matches_per_season = list(Matches.objects.values_list('season').\
        annotate(total_matches = Count('season')).order_by('season'))
    no_of_matches_per_season = dict(no_of_matches_per_season)
    return JsonResponse(no_of_matches_per_season)

# template for problem_one
@cache_page(CACHE_TTL)
def problem_one_template(request):
    return render(request, 'ipldataset/problem_one.html')

# api for problem_two
@cache_page(CACHE_TTL)
def problem_two(request):
    no_of_matches_win_per_team_per_season_in_json ={}
    no_of_matches_win_per_team_per_season = list(Matches.objects.values('season', 'winner').\
        annotate(win= Count('winner')).order_by('season','winner'))
    no_of_matches_win_per_team_per_season_in_json['data'] = no_of_matches_win_per_team_per_season
    return JsonResponse(no_of_matches_win_per_team_per_season_in_json)

# template for problem_two
@cache_page(CACHE_TTL)
def problem_two_template(request):
    return render(request, 'ipldataset/problem_two.html')

# api for problem_three
@cache_page(CACHE_TTL)
def problem_three(request):
    no_of_extra_run_conceded_in_json = {}
    no_of_extra_run_conceded = list(Deliveries.objects.values('bowling_team').filter(match_id__season=2016).\
        annotate(extra_runs = Sum('extra_runs')).order_by('extra_runs'))
    no_of_extra_run_conceded_in_json['data'] = no_of_extra_run_conceded
    return JsonResponse(no_of_extra_run_conceded_in_json)

# template for problem_three
@cache_page(CACHE_TTL)
def problem_three_template(request):
    return render(request, 'ipldataset/problem_three.html')

# api for problem_four
@cache_page(CACHE_TTL)
def problem_four(request):
    top_economy_bowler_in_2015_in_json = {}
    top_economy_bowler_in_2015 = list(Deliveries.objects.values('bowler').filter(match_id__season=2015).\
        annotate(economy=((Sum('total_runs') - Sum('legbye_runs') - Sum('bye_runs')) * 6.0 / Count('ball'))).\
            order_by('economy')[:10])
    top_economy_bowler_in_2015_in_json['data'] = top_economy_bowler_in_2015
    return JsonResponse(top_economy_bowler_in_2015_in_json)


# template for problem four
@cache_page(CACHE_TTL)
def problem_four_template(request):
    return render(request, 'ipldataset/problem_four.html')

# api for problem_five
@cache_page(CACHE_TTL)
def problem_five(request, year):
    toss_win_in_every_season_in_json = {}
    toss_win_in_every_season = list(Matches.objects.filter(season = year).values('toss_winner').\
        annotate(win = Count('toss_winner')))
    toss_win_in_every_season_in_json['data'] = toss_win_in_every_season
    return JsonResponse(toss_win_in_every_season_in_json)

def problem_five_template(request, year):
    return render(request, 'ipldataset/problem_five.html', {'year' : year})

# api for Matches
@cache_page(CACHE_TTL)
@csrf_exempt
def matches(request):
    if request.method == 'GET':
        no_of_matches_in_json = {}
        no_of_matches = list(Matches.objects.values())
        no_of_matches_in_json['data'] = no_of_matches
        return JsonResponse(no_of_matches_in_json)
    if request.method == 'POST':
        request_body = request.body.decode(encoding = 'utf-8')
        request_data = json.loads(request_body)
        match_object = Matches()
        for key in request_data:
            if hasattr(match_object, key):
                setattr(match_object, key, request_data[key])
            else:
                raise Exception(f'invalid data {key}')
        match_object.save()
        return JsonResponse({'status':201})



# api for matches detail
@cache_page(CACHE_TTL)
@csrf_exempt
def match_details(request, match_id):
    if request.method == 'GET':
        match_details_in_json = {}
        match_details = list( Matches.objects.values().filter(id = match_id))
        match_details_in_json['data'] = match_details
        return JsonResponse(match_details_in_json)

    if request.method == 'PUT':
        matches_data = Matches.objects.get                                                                                                                                                                              (id = match_id)
        request_body = request.body.decode(encoding = 'utf-8')
        request_data = json.loads(request_body)
        for key in request_data:
            if hasattr(matches_data, key):
                print(key)
                setattr(matches_data, key, request_data[key])
            else:       
                raise Exception(f'invalid data {key}')
        matches_data.save()
        return JsonResponse({'status' : 203})

    if request.method == 'DELETE':
        Matches.objects.filter(id = match_id).delete()
        return JsonResponse({'status':202})

# api for showing matches in every season
@cache_page(CACHE_TTL)
@csrf_exempt
def match_details_by_season(request, season):
    if request.method == 'GET':
        matches_in_season_in_json = {}
        matches_in_season = list(Matches.objects.values().filter(season = season))
        matches_in_season_in_json['data'] = matches_in_season
        return JsonResponse(matches_in_season_in_json)

# api for matches Deliveries
@cache_page(CACHE_TTL)
@csrf_exempt
def deliveries(request):
    if request.method == 'GET':
        deliveries_in_json = {}
        deliveries = list(Deliveries.objects.values())
        deliveries_in_json['data'] = deliveries
        return JsonResponse(deliveries_in_json)

@cache_page(CACHE_TTL)
@csrf_exempt
def delivery_details(request, delivery_id):
    if request.method == 'GET':
        deliveries_per_ball_in_json = {}
        deliveries_per_ball = list(Deliveries.objects.values().filter(id = delivery_id))
        deliveries_per_ball_in_json['data'] = deliveries_per_ball
        return JsonResponse(deliveries_per_ball_in_json)

